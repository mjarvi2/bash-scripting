#find and replace strings within files of current directory

echo "Enter the string you want to replace"

read toreplace

echo "Enter the string you want to replace it with"

read replacement

find . -type f -exec grep -l "$toreplace" {} \; | xargs sed -i "s/$toreplace/$replacement/g"
