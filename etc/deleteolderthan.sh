#delete files older than a certain number of days

echo "Delete files older than how many days?"

read days

find . -type f -mtime +$days -exec rm {} \;
