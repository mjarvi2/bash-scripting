#!/bin/bash

#script to get the time needed to complete a task

echo "enter name of file you would like to get code execution time of"

read file

#EXECUTE A SCRIPT BELOW
if [[ "$file" != "executiontime.sh" ]]; then 
	start=$(date +%s)
	./$file
	end=$(date +%s)
	elapsed=$((end-start))
	echo "Time elapsed: $elapsed seconds"
else
	echo "You cannot call the name of this script!"
fi
