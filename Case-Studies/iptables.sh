#!/bin/bash

# ACCEPT all in case something goes wrong with the script.
# We don't want to be locked out!
sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

# Flush all previous rules
sudo iptables -F
sudo iptables -X
sudo ip6tables -F
sudo ip6tables -X

# ...

# --- INPUT chain ---
# Drop invalid packets
sudo iptables -A INPUT -m state --state INVALID -j DROP
# Accept all traffic on localhost interface
sudo iptables -A INPUT -i lo -j ACCEPT
# Accept all traffic coming back to us that we already requested
sudo iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
# Allow this system to be pinged (ICMP)
sudo iptables -A INPUT -p icmp -m state --state NEW -j ACCEPT
# Accept SSH connections
sudo iptables -A INPUT -p tcp -m multiport --dports 22,2222 -m state --state NEW -j ACCEPT
# Log all other inbound traffic before it's DROPped
sudo iptables -A INPUT -m limit --limit 6/min -j LOG --log-prefix "[iptables-input] "

# --- OUTPUT chain ---
# ...

# --- Default DROP ---
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP
sudo iptables -P OUTPUT DROP

# --- DROP all IPv6 ---
sudo ip6tables -P INPUT DROP
sudo ip6tables -P FORWARD DROP
sudo ip6tables -P OUTPUT DROP

