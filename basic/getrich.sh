#!/bin/bash

#idea of this taken from Network Chuck's bash scripting video
#Finally figured out how to make it look like strings are changing, like when metasploit is starting.

echo "What is your name?"

read name

echo "what is your age?"

read age

echo "Hello, $name, you are $age years old"

sleep 1
printf "Calculating" && printf '\r\033[1B'
sleep 0.2
printf "cAlculating" && printf '\r\033[1B'
sleep 0.2
printf "caLculating" && printf '\r\033[1B'
sleep 0.2
printf "calCulating" && printf '\r\033[1B'
sleep 0.2
printf "calcUlating" && printf '\r\033[1B'
sleep 0.2
printf "calcuLating" && printf '\r\033[1B'
sleep 0.2
printf "calculAting" && printf '\r\033[1B'
sleep 0.2
printf "calculaTing" && printf '\r\033[1B'
sleep 0.2
printf "calculatIng" && printf '\r\033[1B'
sleep 0.2
printf "calculatiNg" && printf '\r\033[1B'
sleep 0.2
printf "calculatinG" && printf '\r\033[1B' 
echo "Done calculating!"

richat=$((( $RANDOM % 15 ) + $age ))

echo "you will be rich at $richat years old"
